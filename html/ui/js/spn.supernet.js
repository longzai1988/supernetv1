var NRS = (function (NRS, $, undefined) {
    NRS.spnAutoLogin = false;

    $(window).load(function () {
        autoLogin();
    });

    function autoLogin() {
        if ($.trim($("#login_password").val())) {
            NRS.spnAutoLogin = true;
            showAutoLoginPanel();
            setTimeout(function () {
                NRS.login($("#login_password").val());
            }, 1500);
        }
    }

    function showAutoLoginPanel() {
        $("#spn_auto_login").show();
        $("#login_panel").hide();
        $("#welcome_panel").hide();
        $("#account_phrase_custom_panel").hide();
        $("#account_phrase_generator_panel").hide();
        $("#custom_passphrase_link").hide();
    }
    return NRS;
}(NRS || {}, jQuery));